#!/usr/bin/python

# the following script takes in a csv and creates a matrix of frequency of
# which any two classes are taken at the same time within a single semester.
# Then, it formats the resulting adjacency matrix as a JSON object for input to
# Mike Bostock's Les Miserables co-occurency vis.

import json

def work():

    f = open('DMTH-cotaken.csv','r')
    w = open('DMTH.json','w')

    cleanlist = []

    for line in f:
        a,b,c,d,e,f = line.strip().split(',')
        id, course_name, course_term, crn =  a.strip('"')+b+c.strip('"'),d.strip('"'),e.strip('"'),f.strip('"')
        cleanlist.append( [id,course_name,course_term,crn] )

    tree = {}
    for a,b,c,d in cleanlist[:]:
        tree[a] = {}

    for a,b,c,d in cleanlist:
        tree[a][c] = []

    for a,b,c,d in cleanlist:
        tree[a][c].append(b)

    courseset = set()
    for a,b,c,d in cleanlist:
        courseset.add(b)

    print len(courseset)

    cotaken_matrix = {}
    for i in courseset:
        cotaken_matrix[i] = {}

    for key in cotaken_matrix.keys():
        for i in courseset:
            cotaken_matrix[key][i]=0

    for key in tree.keys():
        #print "ID: "+key
        for term in tree[key].keys():
            #print "\tTerm: "+term
            courses = tree[key][term]
            for i in range(0,len(courses)):
                for co in courses[i:]:
                    cotaken_matrix[courses[i]][co] +=1
                    cotaken_matrix[co][courses[i]] +=1
                    if courses[i]==co :
                        print 'double: '+co
                        cotaken_matrix[co][courses[i]] -=1

    for k in cotaken_matrix.keys():
        for l in cotaken_matrix[k].keys():
            if (k==l): cotaken_matrix[k][l] = 0
        if cotaken_matrix[k][l] > 1:
                print (k, l, cotaken_matrix[k][l])
        for id in tree.keys():
            for term in tree[id].keys():
                        if ( k in tree[id][term] and l in tree[id][term]):
                            print ('by|in: ',id,term)

    #{"nodes":[{"name":"Myriel","group":1},{...}],"links":[{"source":1,"target":0,"value":1},{...}]}

    coursesetlist = sorted(list(courseset))
    nodesWorthAdding = set()


    resultnodes = '{"nodes":['
    resultlinks = '"links":['

    for k in cotaken_matrix.keys():
        for l in cotaken_matrix[k].keys():
            if cotaken_matrix[k][l] > 2:
                nodesWorthAdding.add(k)
                nodesWorthAdding.add(l)

    nodesWorthAdding = sorted(list(nodesWorthAdding))

    for c in nodesWorthAdding:
        resultnodes+='{"name":'+json.dumps(c)+',"group":1},'

    resultnodes = resultnodes[:-1]

    for k in cotaken_matrix.keys():
        for l in cotaken_matrix[k].keys():
            if cotaken_matrix[k][l] > 2:
                i,j = nodesWorthAdding.index(k),nodesWorthAdding.index(l)
                resultlinks+='{"source":'+str(i)+',"target":'+str(j)+',"value":'+str(cotaken_matrix[k][l])+'},'

    resultlinks = resultlinks[:-1]
    resultlinks += ']}'

    result = resultnodes+'],'+resultlinks

    w.write(result)
    w.close()

    w = open('testjson.json','w')
    w.write(json.dumps(result))
    w.close()

work()
