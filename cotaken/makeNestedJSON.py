import csv
import json

f = open('BSBIOL','rb')

fieldnames=("id","cohort_group","final_GTGPA","semester_graduated",
            "course_term","course_reference_number","course_code","course_title","grading_mode","grade")

students = []
id_dict = {}

lines = []
for line in f:
    lines.append(line)

f.close()

for line in lines:
    id,c,f,s,cterm,crn,cc,ct,gm,g = line.rstrip().split('","')
    id = id.strip('"')
    g = g.strip('"')
    id_dict[id] = { "cohort_group":c, "final_GTGPA": f, "semester_graduated": s, "terms": {}}

for line in lines:
    id,c,f,s,cterm,crn,cc,ct,gm,g = line.rstrip().split('","')
    id = id.strip('"')
    g = g.strip('"')
    id_dict[id]["terms"][cterm] = []

for line in lines:
    id,c,f,s,cterm,crn,cc,ct,gm,g = line.rstrip().split('","')
    id = id.strip('"')
    g = g.strip('"')
    id_dict[id]["terms"][cterm].append({ "course_reference_number":crn, "course_code":cc,"course_title":ct,"grading_mode":gm,"grade":g})

result = '['

for key in id_dict.keys():
    result+= '{"id":'+key+',"cohort_group":'+id_dict[id]["cohort_group"]+',"final_GTGPA":'+id_dict[id]["final_GTGPA"]
    result+= ',"semester_graduated":'+id_dict[id]["semester_graduated"]+',"terms": ['
    for term,part in id_dict[id]["terms"].items():
        result+= '{"course_term":'+term+',"courses": '+json.dumps(part)+'},'
    result=result[:-1]
    result+=']'

    result+='},\n'

result = result[:-1]
result+=']'

print result



