#!/usr/bin/python
"""Implements 2 ways to access our project data:
    1) as a csv file
    2) as a database connection via sqlite"""


import sqlite3, csv, timeit


def csv_style():
    with open('dataset.csv', 'rb') as csvfile:
        datareader = csv.reader(csvfile, delimiter=',', quotechar='"')
        matches = []
        for row in datareader:
            try:
                sat_writing = int(row[4])
                if sat_writing > 750:
                    matches.append(row)
            except:
                pass


connection = sqlite3.connect('students.db')
cursor = connection.cursor()

def db_style():
    data = cursor.execute('Select * FROM master_table WHERE SAT_writing>'
             + str(750)).fetchall()

if __name__ == '__main__':
    print timeit.timeit(stmt="csv_style()", 
                        number=10, 
                        setup='from __main__ import csv_style')
    print timeit.timeit(stmt="db_style()", 
                        number=10, 
                        setup='from __main__ import db_style')

# EOF
