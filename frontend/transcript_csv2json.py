import json

f = open('onetranscript.csv','r')

transcript = {}
transcript["courses"] = []

for line in f:
	id,coh,hs,s1,s2,s3,hsgpa,gk,intl,major,gpa,g_term,deg,ct,crn,cc,cti,gg,g,alt = line.strip().split('","')
	id1,id2,id3 = id.strip('"').split(',')
	transcript["id"] = id1+id2+id3
	transcript["cohort"] = coh
	transcript["major"]=major
	transcript["gpa"]=gpa
	transcript["degree"] = deg
	transcript["courses"].append({"term":ct,"title":cti,"code":cc,"crn":crn,"grade":g,"gradetype":gg,"transcred":alt})

for cl in transcript["courses"]:
	print cl
	
o = open('onetranscript.json','w')
o.write(json.dumps( transcript ))