(function() {
  var heatmapChart, root;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  heatmapChart = function() {
    var buildRow, chart, height, margin, mouseout, mouseover, onClick, orders, svg, transition_time, width, xScale, xValue, yScale, yValue, zScale, zValue;
    margin = {
      top: 20,
      right: 20,
      bottom: 20,
      left: 20
    };
    width = 800;
    height = 800;
    transition_time = 800;
    xValue = function(d) {
      return d.x;
    };
    yValue = function(d) {
      return d.y;
    };
    zValue = function(d) {
      return parseFloat(d.z);
    };
    xScale = d3.scale.ordinal().rangeBands([0, width]);
    yScale = d3.scale.ordinal().rangeBands([0, height]);
    zScale = d3.scale.linear().range(['#C6DBEF', '#08306B']);
    svg = null;
    orders = {
      x: null,
      y: null
    };
    onClick = function(d, i) {
//       d3.select("svg#foot")
// //       .selectAll("rect").data(d).enter()
// 	  	   .append("rect")
// 		   .attr("id",'eva')
// 		   .attr("x", 10)
// 		   .attr("y", 10)
// 		   .attr("width",100)
// 		   .attr("height", 100)
// 		   .style("fill", d3.rgb(0,255,0))
// 		   .style("stroke", d3.rgb(0,255,0))
// 		   .append("title").text( function(d) { alert(d); return d.z; });
//    //    $('svg#foot').("rect")
// //       	   .attr("id",'eva')
// // 		   .attr("x", 10)
// // 		   .attr("y", 10)
// // 		   .attr("width",100)
// // 		   .attr("height", 100)
// // 		   .style("stroke", d3.rgb(0,255,0))
// // 		   .append("title").text( function(d) { alert(d); return d; });
// 
//       return console.log(d.z);
		var student = [
	    {
      "grade": "T", 
      "course_title": "No Georgia Tech Credit Awarded",  
      "index": 0, 
      "course_term": 200508,
	  "termMaxIndex":8,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "United States since 1877", 
      "index": 1, 
      "course_term": 200508,
	  "termMaxIndex":8,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "English Composition I", 
      "index": 2, 
      "course_term": 200508,
	  "termMaxIndex":8,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Marching Band", 
      "index": 3, 
      "course_term": 200508,
	  "termMaxIndex":8,
	  "studentIndex":0
	}, 
    {
      "grade": "A",  
      "course_title": "Percussion Ensemble", 
      "index": 4, 
      "course_term": 200508,
	  "termMaxIndex":8,
	  "studentIndex":0
    }, 
    {
      "grade": "A",  
      "course_title": "Calculus I", 
      "index": 5, 
      "course_term": 200508,
	  "termMaxIndex":8,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "GT Freshman Seminar", 
      "index": 6, 
      "course_term": 200508,
	  "termMaxIndex":8,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "General Chemistry", 
      "index": 7, 
      "course_term": 200508,
	  "termMaxIndex":8,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Calculus II", 
      "index": 0, 
      "course_term": 200602,
	  "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Percussion Ensemble", 
      "index": 1, 
      "course_term": 200602,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Intro Physics I", 
      "index": 2, 
      "course_term": 200602,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Elec Percuss Studio/Ens.", 
      "index": 3, 
      "course_term": 200602,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "English Composition II", 
      "index": 4, 
      "course_term": 200602,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Computing for Engineers", 
      "index": 5, 
      "course_term": 200602,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Marching Band", 
      "index": 0, 
      "course_term": 200608,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Percussion Ensemble", 
      "index": 1, 
      "course_term": 200608,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Calculus III", 
      "index": 2, 
      "course_term": 200608,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Intro Physics I", 
      "index": 3, 
      "course_term": 200608,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Intro Physics I",
      "index": 4, 
      "course_term": 200608,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Intro to Computer Engr", 
      "index": 5, 
      "course_term": 200608,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "B", 
      "course_title": "Intro Physics I",
      "index": 6, 
      "course_term": 200608,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "B", 
      "course_title": "Intro Physics I",
      "index": 0, 
      "course_term": 200702,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Differential Equations", 
      "index": 1, 
      "course_term": 200702,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Health", 
      "index": 2, 
      "course_term": 200702,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Intro Physics I",
      "index": 3, 
      "course_term": 200702,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Elec Percuss Studio/Ens.", 
      "index": 4, 
      "course_term": 200702,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Intro-Signal Processing", 
      "index": 5, 
      "course_term": 200702,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Statics",  
      "index": 6, 
      "course_term": 200702,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "T", 
      "course_title": "Intro to Sociology", 
      "index": 0, 
      "course_term": 200705,
      "termMaxIndex":2,
	  "studentIndex":0
    }, 
    {
      "grade": "V", 
      "course_title": "Professional Internship", 
      "index": 1, 
      "course_term": 200705,
      "termMaxIndex":2,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Digital Design Lab", 
      "index": 0, 
      "course_term": 200708,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Statistics& Applications", 
      "index": 1, 
      "course_term": 200708,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Marching Band", 
      "index": 2, 
      "course_term": 200708,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Percussion Ensemble", 
      "index": 3, 
      "course_term": 200708,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Circuit Analysis",  
      "index": 4, 
      "course_term": 200708,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Computer Communications", 
      "index": 5, 
      "course_term": 200708,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Prin of Microeconomics", 
      "index": 0, 
      "course_term": 200802,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Instrument& Circuits Lab", 
      "index": 1, 
      "course_term": 200802,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Pep Band", 
      "index": 2, 
      "course_term": 200802,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Electromagnetics", 
      "index": 3, 
      "course_term": 200802,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Microelectronic Circuits", 
      "index": 4, 
      "course_term": 200802,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Internetwork Security", 
      "index": 5, 
      "course_term": 200802,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "W",  
      "course_title": "Microelectro Circuit Lab", 
      "index": 0, 
      "course_term": 200805,
      "termMaxIndex":2,
	  "studentIndex":0
    }, 
    {
      "grade": "V", 
      "course_title": "Professional Internship",  
      "index": 1, 
      "course_term": 200805,
      "termMaxIndex":2,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Emb Microcontroller Dsgn", 
      "index": 0, 
      "course_term": 200808,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Intro Systems & Control", 
      "index": 1, 
      "course_term": 200808,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "D", 
      "course_title": "Marching Band", 
      "index": 2, 
      "course_term": 200808,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Percussion Ensemble", 
      "index": 3, 
      "course_term": 200808,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Biomed Instrumentation", 
      "index": 4, 
      "course_term": 200808,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "F", 
      "course_title": "Ethics in Int'l Affairs", 
      "index": 5, 
      "course_term": 200808,
      "termMaxIndex":6,
	  "studentIndex":0
    }, 
    {
      "grade": "B",  
      "course_title": "Intro-Fluid&Thermal Engr", 
      "index": 0, 
      "course_term": 200902,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "F",  
      "course_title": "Microelectro Circuit Lab", 
      "index": 1, 
      "course_term": 200902,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "C", 
      "course_title": "Pep Band",  
      "index": 2, 
      "course_term": 200902,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Composers 1800-Present", 
      "index": 3, 
      "course_term": 200902,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "B",  
      "course_title": "Computer Arch & Oper Sys", 
      "index": 4, 
      "course_term": 200902,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "Music Recording & Mixing", 
      "index": 5, 
      "course_term": 200902,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "B", 
      "course_title": "Engr Practice & Profess", 
      "index": 6, 
      "course_term": 200902,
      "termMaxIndex":7,
	  "studentIndex":0
    }, 
    {
      "grade": "A", 
      "course_title": "ECE Design Project", 
      "index": 0, 
      "course_term": 200908,
      "termMaxIndex":1,
	  "studentIndex":0
    }
]

    xstart = 0;
    width = 50;
    height = 30;
    ystart = 40;

    var cohorts = [200208, 200302, 200305, 200308, 200402, 200405, 200408, 200502, 200505, 200508, 200602, 200605, 200608, 200702, 200705, 200708, 200802, 200805, 200808, 200902, 200905, 200908, 201002, 201005];
        
    function xpos(d) { return (xstart + width*(cohorts.indexOf(parseInt(d.course_term)))); }
 	function ypos(d) { return ystart + (height/d.termMaxIndex)*d.index + height*d.studentIndex; }
 	function height1(d){ return height/d.termMaxIndex -1;}

	function c(d) {
		if (d.grade == "A")
		 return "#2EFE2E";
		else if (d.grade == "B")
		 return "#9AFE2E";
		else if (d.grade == "C")
		 return "#F7FE2E";
		else if (d.grade == "D")
		 return "#9AFE2E";
		else if (d.grade == "E")
		 return "#FE642E";
		else if (d.grade == "F")
		 return "#FE2E2E";
		else if (d.grade == "W")
		 return "#D8D8D8";
		else if (d.grade == "T")
		 return "#6E6E6E";
	}

	data = student;

	// invoke d3 to select an area
	svg2 = d3.select("svg#foot").selectAll("rect")
	  .data(data).enter().append("rect")
		   .attr("x", xpos)
		   .attr("y", ypos)
		   .attr("width",width)
		   .attr("height", height1)
		   .style("fill", c)
		   .style("stroke", "#F8E6E0")
		   .append("title").text( function(d) { return d.course_title + ' : '+ d.grade; });

	$('rect').hover( function() { d3.select(this).style("stroke", "black"); },
					 function() { d3.select(this).style("stroke", "#F8E6E0"); }
	 );

	$('rect').mousedown( function() { 
	
		var arg = d3.select(this).text().split(':')[0]; 
  		d3.selectAll('rect').filter( function (d) { 
   				return d.course_title == $.trim(arg);
  			})
  			.style("stroke", "blue");
	
	});
        
        $('#main_vis').css('left', 0);
    };
    chart = function(selection) {
      return selection.each(function(data) {
        var column, data_counts, g, gEnter, row, row_text, unique_x_names, unique_y_names;
        data = data.map(function(d, i) {
          var new_data;
          new_data = d;
          new_data.x = xValue.call(data, d, i);
          new_data.y = yValue.call(data, d, i);
          new_data.z = zValue.call(data, d, i);
          return new_data;
        });
        data_counts = {};
        data.forEach(function(d) {
          var _base, _name, _name2;
          if (data_counts[_name = d.x] == null) data_counts[_name] = {};
          if ((_base = data_counts[d.x])[_name2 = d.y] == null) _base[_name2] = 0;
          return data_counts[d.x][d.y] += 1;
        });
        console.log(data_counts);
        unique_x_names = d3.keys(data_counts);
        unique_y_names = {};
        d3.entries(data_counts).forEach(function(e) {
          return d3.keys(e.value).forEach(function(k) {
            var _ref;
            return (_ref = unique_y_names[k]) != null ? _ref : unique_y_names[k] = 1;
          });
        });
        unique_y_names = d3.keys(unique_y_names);
        console.log(unique_y_names);
        orders = {
          x: {
            original: data.map(function(d) {
              return d.x;
            }),
            name_asc: data.map(function(d) {
              return d.x;
            }).sort(function(a, b) {
              return d3.ascending(a, b);
            }),
            name_dsc: data.map(function(d) {
              return d.x;
            }).sort(function(a, b) {
              return d3.descending(a, b);
            })
          },
          y: {
            original: data.map(function(d) {
              return d.y;
            }),
            name_asc: data.map(function(d) {
              return d.y;
            }).sort(function(a, b) {
              return d3.ascending(a, b);
            }),
            name_dsc: data.map(function(d) {
              return d.y;
            }).sort(function(a, b) {
              return d3.descending(a, b);
            })
          }
        };
        xScale.domain(orders.x.original);
        yScale.domain(orders.y.original);
        zScale.domain(d3.extent(data, function(d) {
          return d.z;
        }));
        svg = d3.select(this).selectAll("svg").data([data]);
        gEnter = svg.enter().append("svg").append("g");
        svg.attr("width", width + margin.left + margin.right);
        svg.attr("height", height + margin.top + margin.bottom);
        g = svg.select("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        g.append("rect").attr("class", "background").attr("width", width).attr("height", height);
        row = g.selectAll(".row").data(data).enter().append("g").attr("class", "row").attr("transform", function(d, i) {
          return "translate(" + 0 + "," + (yScale(d.y)) + ")";
        }).each(buildRow);
        row_text = g.selectAll("row_text").data(unique_y_names).enter().append("g").attr("class", "row_text").attr("transform", function(d, i) {
          return "translate(" + 0 + "," + (yScale(d)) + ")";
        });
        row_text.append("line").attr("x1", 0).attr("x2", width).attr("class", "row_line");
        row_text.append("text").attr("x", -6).attr("y", yScale.rangeBand() / 2).attr("dy", ".32em").attr("text-anchor", "end").text(function(d, i) {
          return d;
        });
        console.log(unique_x_names);
        console.log(xScale(unique_x_names[1]));
        column = g.selectAll(".column").data(unique_x_names).enter().append("g").attr("class", "column").attr("transform", function(d, i) {
          return "translate(" + (xScale(d)) + ") rotate(" + (-90) + ")";
        });
        column.append("text").attr("x", 6).attr("y", xScale.rangeBand() / 2).attr("dy", ".32em").attr("text-anchor", "start").text(function(d, i) {
          return d;
        });
        column.append("line").attr("x1", -width).attr("class", "col_line");
        d3.select("#order_row").on("change", function() {
          return chart.order("y", this.value);
        });
        return d3.select("#order_col").on("change", function() {
          return chart.order("x", this.value);
        });
      });
    };
    buildRow = function(row) {
      var cell;
      return cell = d3.select(this).selectAll(".cell").data([row]).enter().append("rect").attr("class", "cell").attr("x", function(d) {
        return xScale(d.x);
      }).attr("width", xScale.rangeBand()).attr("height", yScale.rangeBand()).attr("fill", function(d) {
        return color(d.z);
      }).on("mouseover", mouseover).on("mouseout", mouseout).on("click", onClick).append("title").text(function(d){return 'Average GPA: '+ d.z;});
    };

    
    function color(d){
  		return d3.rgb((255-Math.floor(((d-2.47059)/(3.88596-2.47059))*255)),255,(255-Math.floor(((d-2.47059)/(3.88596-2.47059))*255)));
  	}
    
    mouseover = function(p, i) {
      d3.selectAll(".row_text text").classed("active", function(d, i) {
        return d === p.y;
      });
      return d3.selectAll(".column text").classed("active", function(d, i) {
        return d === p.x;
      });
    };
    mouseout = function(p, i) {
      return d3.selectAll("text").classed("active", false);
    };
    chart.width = function(_) {
      if (!arguments.length) return width;
      width = _;
      return chart;
    };
    chart.height = function(_) {
      if (!arguments.length) return height;
      height = _;
      return chart;
    };
    chart.margin = function(_) {
      if (!arguments.length) return margin;
      margin = _;
      return chart;
    };
    chart.x = function(_) {
      if (!arguments.length) return xValue;
      xValue = _;
      return chart;
    };
    chart.y = function(_) {
      if (!arguments.length) return yValue;
      yValue = _;
      return chart;
    };
    chart.z = function(_) {
      if (!arguments.length) return zValue;
      zValue = _;
      return chart;
    };
    chart.scale = function(_) {
      if (!arguments.length) return zScale;
      zScale = _;
      return chart;
    };
    chart.order = function(axis, value) {
      var delay, scale, t;
      scale = axis === "x" ? xScale : yScale;
      scale.domain(orders[axis][value]);
      t = svg.transition().duration(transition_time);
      delay = 2.5;
      t.selectAll(".row").delay(function(d, i) {
        return yScale(d.y) * delay;
      }).attr("transform", function(d, i) {
        return "translate(0," + (yScale(d.y)) + ")";
      }).selectAll(".cell").delay(function(d) {
        return xScale(d.x) * delay;
      }).attr("x", function(d) {
        return xScale(d.x);
      });
      t.selectAll(".row_text").delay(function(d, i) {
        return yScale(d) * delay;
      }).attr("transform", function(d, i) {
        return "translate(0," + (yScale(d)) + ")";
      });
      return t.selectAll(".column").delay(function(d, i) {
        return xScale(d) * delay;
      }).attr("transform", function(d, i) {
        return "translate(" + (xScale(d)) + ")rotate(-90)";
      });
    };
    return chart;
  };

  root.heatmapChart = heatmapChart;

}).call(this);
