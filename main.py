#!/usr/bin/python
from flask import Flask, jsonify, render_template, request, g
from collections import defaultdict
import json_app
import json
import sqlite3
import string


app = json_app.make_json_app('__main__')

DATABASE = 'students.db'

def connect_db():
    return sqlite3.connect(DATABASE)

def query_db(query, args=(), one=False):
    cur = g.db.execute(query, args)
    rv = [dict((cur.description[idx][0], value)
               for idx, value in enumerate(row)) for row in cur.fetchall()]
    return (rv[0] if rv else None) if one else rv

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    if hasattr(g, 'db'):
        g.db.close()

def query_builder(values_dict):
    queries = []

    ranges = [('final_GTGPA',(1.92,4)), ('HSGPA',(2,4)),
              ('SAT_math',(400,800)), ('SAT_verbal',(340,800)),
              ('SAT_writing',(400,800))]

    for range_type, values in ranges:
        min_val, max_val = tuple(float(x) for x in values_dict[range_type].split(' - '))
        if values[0] < min_val and values[1] > max_val:
            queries.append(range_type + ' between ' + str(min_val) + ' and ' + str(max_val))
        elif values [0] < min_val and values[1] == max_val:
            queries.append(range_type + ' > ' + str(min_val))
        elif values [0] == min_val and values[1] > max_val:
            queries.append(range_type + ' < '  + str(max_val))

    if values_dict['cohort_group'] != '':
        queries.append('cohort_group=' + values_dict['cohort_group'])

    if values_dict['highschool'] != '':
        queries.append('highschool=\'' + values_dict['highschool'] + '\'')

    if values_dict['major'] != '':
        queries.append('degree=\'' + values_dict['major'] +'\'')

    if values_dict['greek'] == 'yes':
        queries.append('greek=1')
    elif values_dict['greek'] == 'no':
        queries.append('greek<>1')

    if values_dict['international'] == 'yes':
        queries.append('international=1')
    elif values_dict['international'] == 'no':
        queries.append('international<>1')

    query = 'select * from master_table where '
    for idx, q in enumerate(queries):
        if idx < len(queries)-1:
            query += q + ' and '
        else:
            query += q

    # Apply subquery
    #query = 'select ' + query + ')'

    return query

def parse_request(request):
    values_dict = {}
    value_names = ['major', 'highschool',
              'cohort_group', 'final_GTGPA',
              'HSGPA', 'SAT_math',
              'SAT_verbal', 'SAT_writing',
              'international', 'greek']

    for v in value_names:
        values_dict[v] = request.args.get(v)

    return values_dict

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/_record_count')
def get_record_count():
    values_dict = parse_request(request)
    student_rows = query_db(query_builder(values_dict))

    return jsonify(record_count=len(student_rows))

@app.route('/_get_students')
def get_students():
    values_dict = parse_request(request)
    student_rows = query_db(query_builder(values_dict))

    curr_student = ""
    curr_course_term = ""
    id_dict = defaultdict(lambda: defaultdict(list))
    for row in student_rows:
        if curr_student != row['id']:
            curr_student = row['id']
            id_dict[row["id"]] = { "cohort_group":row["cohort_group"], "final_GTGPA": row["final_GTGPA"], "semester_graduated": row["semester_graduated"],"terms":[]}

        if curr_course_term != row['course_term']:
            curr_course_term = row['course_term']
            courses_list = []
            id_dict[row["id"]]["terms"].append({"course_term":row["course_term"],"courses":courses_list,"cohort_group":row["cohort_group"]})

        courses_list.append({ "course_reference_number":row["course_reference_number"], "course_code":row["course_code"],"course_title":row["course_title"],"grading_mode":row["grading_mode"],"grade":row["grade"]})

    return json.dumps([id_dict])

@app.route('/_get_student')
def get_student():
    student_id = request.args.get('student_id')

    #query = 'select * from master_table where id=\'' + student_id + '\''
    query = 'select * from master_table where id=\'' + student_id + '\' ORDER BY course_term'

    student_rows = query_db(query)

    curr_course_term = ''
    courses = []
    for row in student_rows:
        if curr_course_term != row['course_term']:
            curr_course_term = row['course_term']
            course_term_idx = 0

        course_dict = {'course_title':row['course_title'],
                       'course_code':row['course_code'],
                       'course_reference_number':row['course_reference_number'],
                       'course_term':row['course_term'],
                       'grade':row['grade'],
                       'grading_mode':row['grading_mode'],
                       'credit_from':row['credit_from'],
                       'index':course_term_idx}
        courses.append(course_dict)
        course_term_idx = course_term_idx +1

    student_row = student_rows[0]

    return jsonify(cohort_group=student_row['cohort_group'],
                   degree_major=student_row['degree_major'],
                   SAT_math=student_row['SAT_math'],
                   SAT_verbal=student_row['SAT_verbal'],
                   SAT_writing=student_row['SAT_writing'],
                   HSGPA=student_row['HSGPA'],
                   final_GTGPA=student_row['final_GTGPA'],
                   courses=courses)

@app.route('/heatmap')
def heatmap():
    return render_template('heatmap.html')

@app.route('/_cotaken_matrix')
def cotaken_matrix():
    #[id,course_name,course_term,crn]
    #{"nodes":[{"name":"Myriel","group":1},{...}],"links":[{"source":1,"target":0,"value":1},{...}]}

    values_dict = parse_request(request)
    query = query_builder(values_dict)
    student_rows = query_db(query)
    cotaken_dict = create_cotaken_dict(student_rows)

    keylist = cotaken_dict.keys()
    keylist.sort()
    nodeList = []
    linkList = []

    for key in keylist:
       node_dict = {}
       node_dict["name"] = key
       node_dict["group"] = 0
       nodeList.append(node_dict)
       for key2 in cotaken_dict[key].keys():
          link_dict = {}
          link_dict["source"] = keylist.index(key)
          link_dict["target"] = keylist.index(key2)
          link_dict["value"] = str(cotaken_dict[key][key2])
          linkList.append(link_dict)

    result = {}
    result["nodes"] = nodeList
    result["links"] = linkList

    return json.dumps(result)

def create_cotaken_dict(student_rows):
    curr_student = ""
    curr_course_term = ""
    courses = []
    cotaken = defaultdict(lambda: defaultdict(int))

    for row in student_rows:
        if curr_student != row['id'] or curr_course_term != row['course_term']:
            curr_student = row['id']
            curr_course_term = row['course_term']
            process_courses(courses, cotaken)
            courses = []

        courses.append(row['course_code'])

    return cotaken

def process_courses(courses, cotaken):
    courses.sort()
    for i in courses:
        for j in courses:
            if i == j:
                continue
            cotaken[i][j] = cotaken[i][j] + 1


if __name__ == '__main__':
    app.run(debug=True)
